from django.db import models


class Event(models.Model):
    name = models.CharField(max_length=500)
    description = models.CharField(max_length=1000)
    author = models.CharField(max_length=200)
    north = models.BooleanField()
    south = models.BooleanField()
    begin_time = models.DateTimeField()
    end_time = models.DateTimeField()
    layout = models.IntegerField(default=1)
    approved = models.BooleanField(default=0)


