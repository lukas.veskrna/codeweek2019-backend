from rest_framework import serializers

from .models import Event


class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = (
            'id',
            'name',
            'description',
            'author',
            'north',
            'south',
            'begin_time',
            'end_time',
            'layout',
            'approved',
        )
        read_only_fields = ('id',)
